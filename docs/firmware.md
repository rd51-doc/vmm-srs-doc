# Firmware

As the reader may know, the SRS needs a specific FPGA firmware for
each front-end ASIC.
In case of VMM3a/SRS the system has *two* FPGAs (one on the FEC and
one on the hybrid) which require individual firmware versions.

For (re-)programming the FPGAs (and to generate the `.mcs` files for a
permanent flashing), see the
[firmware upgrade tutorial](firmware/FEC_3_6_Firmware_Upgrade_Tutorial (3)(1).pdf)
and the [VMM hybrid configuration](https://drive.google.com/file/d/1VZJY-wE49nfGjuopBCZCbm7NnMJyR7mb/view?usp=sharing).


!!! danger "Validity of the following statements"

    All the statements made in the following on FPGAs and EEPROMS for flashing are only valid for FECv6 with the PCB design from May 2014 (see the image on the production date --- which is written on the PCB for the FECs that are working with VMM3a/SRS --- for the meaning of the design date).

!!! warning "Production date"

    The production date for the FEC (pre 2019 or 2019+) is **NOT** the
    text written on the PCB.
    This is the design date of the FEC. The production date is written
    on a little white sticker on the PCB:
    ![](firmware/image_fec-version-sticker.png)
    In this example, the FEC was produced in October 2017 and not in
    May 2014!

!!! warning "No sticker on the FEC"

    This gets admittedly a bit tricky. The easiest would be to check when you bought the FEC from the CERN Store or SRS Technology (before or after 2019).

    However, you can also just try to flash the FEC first with the pre 2019 flash settings. If it works, great. If it fails, you know that you have to take the other flash settings.
    However, ideally this should be your last option. Please try the other options before!

!!! note "MAC addresses"

    FEC MAC address as from 2015 (MAC address label and EEPROM):`00.50.C2` </br>
    Default FEC IP: `10.0.0.2` </br>
    Old MAC addresses (type `08-00-30-F2-01-00`) must be reprogrammed </br>
    Serial Numbers: SAMWAY, see FEC EEPROM programming instructions </br>


#### Bit and mcs files to flash the FPGAs with
The `.bit` files to generate the `.mcs` can be found here.
Also the `.mcs` files are provided.
They should work with your setup. If they do not work, you might need to generate them again by yourself using the instructions given above.

* Large systems: *(default, **44.4444 MHz base clock**, 22.5 ns period)*
    * [FEC (bit)](firmware/1e58934_44MHz_FEC.bit)
    * [FEC (mcs)](firmware/fec44MHz.mcs)
    * [Spartan-6 hybrid (bit)](firmware/top_hybrid_20210415.bit)
    * [Spartan-6 hybrid (mcs)](firmware/hybrid44MHz.mcs)
    * [Spartan-7 hybrid (bit)](firmware/Spartan7_vmm3a_hybrid_44p4.bit)
    * [Spartan-7 hybrid (mcs)](firmware/Spartan7_vmm3a_hybrid_44p4.mcs)

* Small flavour: *(**40 MHz base clock**, 25.0 ns period)*
    * [FEC (bit)](firmware/fecv6_vmm3_top_21031200.bit)
    * [Spartan-6 hybrid (bit)](firmware/vmm3h_1_020920_20201118.bit)

Alternatively, the firmware files can be also found in the
repositories of the corresponding control software flavour.


#### Settings for flashing the FPGAs

!!! danger " Xilinx Impact and Vivado"

    Due to choices where the developers have no influence on,
    the FEC and the Spartan-6 hybrids have to be programmed using Xilinx iMPACT,
    while the Spartan-7 hybrids have to be programmed using Xilinx Vivado!

The following settings should be selected for **Xilinx iMPACT**:

* SPI flash
* Hybrids, select `AT45DB161E` (the SPI flash with 'E' at the end)
* FECs produced before 09/2019 (see the sticker on the FEC stating the
  data), select `S25FL064P` in Xilinx iMPACT
* FECs produced after 09/2019, select `N25Q128 1.8/3.3 V`

Although the hybrids should have a working firmware flashed, the
reader may want to upload a more recent version.
If so, a JTAG adapter is needed.
This can be purchased via SRS technology.

For programming the Spartan-7 hybrids with the corresponding settings using **Xilinx Vivado**, see [here](firmware/flashing-spartan-7.pdf).