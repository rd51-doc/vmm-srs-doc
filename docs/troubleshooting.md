# Troubleshooting

As first rule for the troubleshooting, please follow "Muller's Law":<br>
§1: if a bug is declared as HW look for a bug in FW or SW<br>
§2: if FW or SW declares to be bug free go to §1<br>

Or in other words: the hardware that you have should be fine. If you encounter some strange
behaviour, first check that the software works correctly, that you have the correct firmware
loaded, and that fundamental hardware "standards" (e.g. good grounding, which is super important for VMM3a/SRS)
are fulfilled.

!!! warning "**READ THE INSTRUCTIONS**"

    Most encountered problems have already been discovered and described here.
    Please take part of the error message and use the search function.

    Apparently most problems are encountered during the installation of the ESS DAQ.
    Again, the ESS DAQ is some feature that you **can** use, but **you are not obliged to install it**.
    It is a great tool for online monitoring, but completely irrelevant for controlling the system, taking the data or analysing the data.
    For this you need the slow control, tcpdump and vmm-sdat!

[**Slow Control troubleshooting**](./troubleshooting/slowcontrol.md)

[**Conan troubleshooting**](./troubleshooting/conan.md)

[**Other troubleshooting**](./troubleshooting/other.md)

[**Legacy: ESS DAQ / EFU troubleshooting**](./troubleshooting/essdaq.md)

[**Legacy:ESS DAQ / Kafka troubleshooting**](./troubleshooting/kafka.md)

[**Legacy:ESS DAQ / DAQuiri troubleshooting**](./troubleshooting/daquiri.md)
