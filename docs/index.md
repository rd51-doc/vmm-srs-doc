# Welcome

Welcome to the documentation and guide on the VMM3a front-end inside
the RD51 Scalable Readout System (SRS).

This guide evolves continuously, as the readout system itself evolves
continuously.
It will provide you with the information on hardware, firmware and
software related to VMM3a/SRS.
It can happen that not all information are directly saved on this
webpage, but that you will be linked to other webpages or
repositories, where you will then find the information you are looking
for.

In case you are looking for published references, please finde them
as follows:

**Articles on VMM3a/SRS** *(please cite at least one of them, in the case you are using VMM3a/SRS)*

\[1\] L. Scharenberg et al., *Development of a high-rate scalable readout system for gaseous detectors*, Journal of Instrumentation **17** (2022) C12014. [https://doi.org/10.1088/1748-0221/17/12/C12014](https://doi.org/10.1088/1748-0221/17/12/C12014)

\[2\] D. Pfeiffer et al., *Rate-capability of the VMM3a front-end in the RD51 Scalable Readout System*, Nuclear Instruments and Methods in Physics Research A **1031** (2022) 166548. [https://doi.org/10.1016/j.nima.2022.166548](https://doi.org/10.1016/j.nima.2022.166548)

\[3\] M. Lupberger et al., *Implementation of the VMM ASIC in the Scalable Readout System*, Nuclear Instruments and Methods in Physics Research A **903** (2018) 91-98. [https://doi.org/10.1016/j.nima.2018.06.046](https://doi.org/10.1016/j.nima.2018.06.046)


**Detailed information on the full structure of VMM3a/SRS, its working principle, the calibration and optimisation procedures, as well as example measurements and performance studies**

\[4\] L. Scharenberg, *Next-Generation Electronics for the Read-Out of Micro-Pattern Gaseous Detectors*, PhD Thesis, Rheinische Friedrich-Wilhelms-Universität Bonn (2022), CERN-THESIS-2022-360. [https://cds.cern.ch/record/2860765/](https://cds.cern.ch/record/2860765/)


**Articles and information on the VMM3a ASIC itself**

\[5\] G. de Geronimo et al., *The VMM3a ASIC*, IEEE Transactions on Nuclear Science **69** (2022) 976-985. [https://doi.org/10.1109/TNS.2022.3155818](https://doi.org/10.1109/TNS.2022.3155818)

\[6\] G. Iakovidis, *The VMM3a User Guide*, ATL-MUON-PUB-2022-002 (2022). [https://cds.cern.ch/record/2807691](https://cds.cern.ch/record/2807691)


**Tutorial videos on the VMM3a/SRS operation**

Two tutorial videos on the VMM3a/SRS operation exist.
One recorded during the RD51 Collaboration Meeting in 2021 and another one recorded during the RD51 Micro-Pattern Gaseous Detectors School in 2023.
The video from 2021 contains legacy software versions and is not fully up to date anymore.
The video from 2023 is combined with a general introductory lecture on the SRS electronics.

\[7\] L. Scharenberg, *SRS/VMM3a - Documentation and software-operation overview*, RD51 Collaboration Meeting and Topical Workshop "Wide Dynamic Range Operation of MPGDs" (2021). [https://indico.cern.ch/event/1071632/contributions/4615369/](https://indico.cern.ch/event/1071632/contributions/4615369/attachments/2345888/4000941/VMM-SRS-SoftwareInstallation-Scharenberg.mp4)

\[8\] M. Lupberger, *RD51 Scalable Readout System - Demonstration*, RD51 Micro-Pattern Gaseous Detectors School (2023). [https://indico.cern.ch/event/1239595/contributions/5418456/](https://indico.cern.ch/event/1239595/contributions/5418456/attachments/2763115/4812298/SRSDemonstration-Lupberger.mp4)


#### Collaborative effort
As the readout system and all its related components, including this
documentation, are a collaborative effort, you are invited to also
participate in the collaborative activities.

Originally, this was performed within the context of the
Working Group 5.1 within the RD51 Collaboration.
With the end of RD51 in 2023, these activities became part of the
Working Group 5 of the DRD1 Collaboration:

* [DRD1 Working Group 5 meetings](https://indico.cern.ch/category/16511/)

Nonetheless, you can have a look at the previous RD51 Meetings:

* [Working Group 5.1 meetings](https://indico.cern.ch/category/11660/) for everybody who is using/wants to use VMM3a/SRS.

Because of the transition of RD51 to DRD1, please do NOT subscribe to the
former [RD51 WG5.1 mailing list](https://e-groups.cern.ch/e-groups/Egroup.do?egroupName=rd51-wg51) any more, but subscribe to the
[DRD1 WG5 list](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10550545).

In addition, we offer a community forum to exchange about issues and problems:

* [VMM3a/SRS Discord server](https://discord.gg/jsT9HgD)


#### Authors of this guide
[*Lucian Scharenberg,*](mailto:lucian.scharenberg@cern.ch)
Dorothea Pfeiffer, Hans Muller <br>
with contributions from Karl Flöthner, Gerardo Romero, Tassos Belias,
Daichi Nagasaki, Clement Risso and Marco Sessa


#### Acknowledgements
* This work has been sponsored by the Wolfgang Gentner Programme of the German Federal Ministry of Education and Research (grant no. 13E18CHA).
* This project has received funding from the European Union's Horizon 2020 Research and Innovation programme under Grant Agreement No 101004761.
* The work has been supported by the CERN Strategic Programme on Technologies for Future Experiments (https://ep-rnd.web.cern.ch/).

