# Hardware components

For a detailed description of the hardware components of the system it is referred to
the [VMM3a/SRS thesis, chapter 2](https://cds.cern.ch/record/2860765/) and the
[Google Drive of Hans Muller](https://drive.google.com/drive/folders/1h5KLMAa7-bxbPoisnip5Adb4mYc2YMdq).

Here, the components are also shown, but more in view on how to operate them.


#### RD51 VMM hybrid: which HDMI port to use?

On the left, the HDMI cable, the external power port (AUX connector) and the
grounding can be seen (from left to right).
The right picture shows the back of a Hybrid.

!!! danger "Only one HDMI port is connected in the firmware"

    Please be aware that only one HDMI port is currently implemented in
    the firmware.
    It is the left one in the top view (you look at the black passive cooler).

| ![](./image_Hybrid_front.png) |
|:--:|
| Top side of a hybrid |

| ![](./image_Hybrid_back.png) |
|:--:|
| Bottom side of a hybrid |


#### RD51 VMM hybrid: power pins

The hybrid needs to voltages for a correct operation.
P1 for the FPGA, the external ADC, the flash memory, etc. and P2 for the VMM3a ASICs.

* **P1 = 3.05 to 3.45 V**
* **P2 = 1.85 to 2.15 V**

These are the values at the hybrid. Please measure the power there and not on the DVMM.
The power on the DVMM will be higher due to losses in the HDMI cables.

| ![](./hybrid-on-detector.jpg) |
|:--:|
| Power pins of the hybrid |


#### RD51 VMM hybrids: mounted at detector

Here an example of a 10 x 10 cm² triple GEM detector is shown, with 4 hybrids
being used to read it out.

| ![](./image_10x10_with_VMM.png) |
|:--:|
| 10 x 10 cm² detector with connected hybrids |

#### DVMM card

The DVMM card is made for 8 hybrids and can power each via an HDMI port.
One can also use the upper right ports for separate powering.

| ![](./dvmm.jpg) |
|:--:|
| DVMM card for 8 Hybrids |


#### SRS PowerCrate 2k

Here the full configuration of an SRS Powercrate 2k is shown.
It includes 2 FECs, 2 DVMMs and for each of those pairs 8 hybrids (so 2048 channels).

| ![](./image_SRS_mini_front.png) |
|:--:|
| Front of the SRS crate |

| ![](./image_SRS_mini_back.png) |
|:--:|
| Backside of the SRS crate |


#### FPGAs and EEPROMs used on the hybrids and the FEC

The programmable devices (flash memory for the FPGAs) are (here are
only the product numbers mentioned):

* FEC v6 (Virtex 6) `XC6VLX130T-1FFG784C`  
  pre 2019 -> 64 Mbit `S25FL064P0XNFI001`  
  2019+    -> 128 Mbit `MT25QL128ABA`
* RD51 VMM hybrid (Spartan 6) `XC6SLX16-2CSG225C`  
  Boot Flash (until 2020)	-> 16 Mbit `AT45DB161E`  
  Boot Flash (from 2021) 	-> 32 Mbit `AT45DB321E-SHF2B-T`  
  Auxiliary Flash/Serial Nr: 4 kbit `AT24CS04-STUM`
* RD51 VMM hybrid (Spartan 7) `XC7S25-1CSGA324C`

For the production dates of the FEC (pre 2019 and 2019+) see the [firmware section](https://vmm-srs.docs.cern.ch/firmware/).
