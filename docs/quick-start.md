# Quick start

#### Where to find what?

* **Hardware**: There is the [Google Drive](https://drive.google.com/drive/folders/1h5KLMAa7-bxbPoisnip5Adb4mYc2YMdq)
  from Hans Muller, where all hardware components are described.
  In addition, please have a look at the hardware section of this
  guide.
* **Firmware**: Quick links to the firmware:
    * [FEC firmware](firmware/fec44MHz.mcs)
    * [Spartan-6 hybrid firmware](firmware/hybrid44MHz.mcs) (for hybrids received **before** about May 2023)
    * [Spartan-7 hybrid firmware](firmware/Spartan7_vmm3a_hybrid_44p4.mcs) (for hybrids received **after** about May 2023)
  For more information on the firmware check out the [firmware section of this guide](https://vmm-srs.docs.cern.ch/firmware/).
* **Software**: The software contains different components, as
    * the [control software:](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/tree/large_systems)
    * the [data acquisition software](https://www.tcpdump.org/)
    * the [online monitoring](https://github.com/ess-dmsc/vmm-sdat/tree/main/monitoring),
      which is automatically shipped together with the cluster reconstruction software
    * the [cluster reconstruction software](https://github.com/ess-dmsc/vmm-sdat)


#### System requirements

The hardware components for a minimal system of VMM3a/SRS are (they can be
all purchased via SRS technology, with the exception of a DAQ computer):

* RD51 VMM hybrids
* HDMI cables
* Ground return cables
* Digital VMM adapter (DVMM) card
* Front-End Concentrator (FEC) card
* SRS Power Crate 2k
* Ethernet cable
* DAQ computer

The DAQ computer should have the following requirements

* Ubuntu 18.04 LTS or 22.04 LTS (freshly installed without any additions)
* minimum 8 GB of RAM (32 GB of RAM in case you also want to do the
  data analysis on this machine)
* minimum 4 cores and 8 threads CPU
* minimum 500 GB SSD (2 TB SSD is recommended)
* 10 Gbps network card with support for Jumbo-frames (Intel X710T2L was tested and works)
* 10 Gbps network switch (Netgear GS110MX was tested and works)

In case of questions, please checkout the troubleshooting page.

!!! Warning "Use Ubuntu on a physical machine"

    It is understandable that, due to requirements from your institute
    or your personal preferences, you might not want to use Ubuntu as
    operating system.
    However, all the installation and documentation procedure has been
    optimised towards Ubuntu on a dedicated PC (the default Ubuntu,
    not Xubuntu or Kubuntu or any other flavour, and not a virtual
    machine or the WSL in Windows).
    This means that in case you decide to nonetheless deviate from the
    recommendation and use a different operating system, there is no
    support provided.