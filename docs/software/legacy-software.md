# Legacy software

#### Software (required)

* RD51 VMM slow control software (can be found here:
  [Slow Control](https://gitlab.cern.ch/rd51-slow-control/vmmsc)).
* To use the slow control, Qt has to be installed on the computer.
  **Version 5.12.9** was found to be working well.

#### Legacy software (recommended)

Additionally the following software for data
taking, data analysis and online monitoring _can_ be used:

* Wireshark
* DAQ and online monitoring software from the European Spallation
  Source [(ESS)](https://github.com/ess-dmsc/essdaq).
  It consists out of the _Event Formation Unit_ (EFU) for the online
  cluster reconstruction and _DAQuiri_ for the graphical
  representation of these clusters.
* Tcpdump to record at high data rates:
  [Tcpdump](https://github.com/the-tcpdump-group/tcpdump)
* vmm-sdat for the data analysis:
  [vmm-sdat](https://github.com/ess-dmsc/vmm-sdat).

!!! danger "Operating system"

    It is **highly** recommended to use Ubuntu 18.04 as operating
    system, running on a physical machine!

    Any other Linux distribution or Ubuntu version was not tested.
    If you want to use a different distribution, you have to test it
    by yourself and not rely on the help of the experts.

    Trying to install the software in a virtual machine, a docker
    image or the WSL has been tested several times, due to the
    interest of the collaborators, but so far no successfully running
    system was set up.
    This means: if you decide to go down this path, the collaboration
    will be certainly interested in your results, but please do not
    expect support from the experts. They recommend to use
    Ubuntu 18.04 LTS on a physical machine.


It is also recommended to use gcc/g++ in version 9 and CMake in
version 3.21.X.
For the installation of gcc/g++ in the relevant version see the slow
control [repository](https://gitlab.cern.ch/rd51-slow-control/vmmsc),
as this should be the starting point for any VMM3a/SRS related
software installation.

#### Legacy software (further information)
In addition to this, it should be noted, that the VMM slow control
(will be shown later on) has an integrated VMM test module.
The data from the VMM tests can be stored in a data base, of which
the data base browser can be found here:
https://github.com/FinnJaekel/VMM-Database-Browser


#### Legacy software installation remarks

In the following a brief overview on the software installation procedure is given.
For troubleshooting, see the troubleshooting directory.

!!! warning "Installation order"

    It should be noted that the installation order and the correct
    fulfillment of the needed software requirements are important.
    This means:

    * Do not install the software components in a different order then
      the one described here. You might miss some dependencies.
    * The installation procedure was only tested on Ubuntu 18.04 LTS.
      If you try any other operating system or version, you might run
      into problems, which are not part of this documentation.
    * When you change your ``.bashrc`` file, do not forget to
      ``source`` it or just restart your computer.
    * It is recommended to read through the entire installation
      procedure AND the troubleshooting part **before** actually
      starting the installation.

!!! note "Stability of software versions"

    It is **NOT** guaranteed that the version (a certain commit
    in a certain branch) of one software component works well with the
    version of another software component.
    With the Slow Control, vmm-sdat and tcpdump less problems are
    expected. The main possible error source for a "commit clash"
    could occur with the EFU and DAQuiri.

You might need to adjust the commit versions of the software during
the installation (see in point 7 of the installation order on how to
revert to certain commits).
However, the following links should provide you versions that have
been tested successfully on Ubuntu 18.04 LTS:

* [ESS DAQ general](https://gitlab.cern.ch/rd51-doc/ess-daq-fork)
* [EFU](https://gitlab.cern.ch/rd51-doc/efu-fork)
* [DAQuiri](https://gitlab.cern.ch/rd51-doc/daquiri-fork)

For the other components (slow control, vmm-sdat, tcpdump), you can
select the latest commit version.


#### Legacy installation order

1. Make sure that ``git`` and ``make`` are installed. If they are not
   installed, install them via
   ```bash
   sudo apt install git make
   ```
2. Perform the ``git clone`` commands of the software
   ```bash
   git clone https://gitlab.cern.ch/rd51-slow-control/vmmsc
   git clone https://github.com/ess-dmsc/essdaq
   git clone https://github.com/ess-dmsc/vmm-sdat
   ```
   For ``essdaq``, you can also use
   ```bash
   git clone https://gitlab.cern.ch/rd51-doc/ess-daq-fork
   ```
   Then you do not have to revert to a specific commit later on.
3. Install ``cmake`` from the Ubuntu software centre (snap package).
   The installation was successfully tested with version ``3.21.4``
   of ``cmake``.
4. Create an alias in the ``.bashrc`` for cmake:
   ```bash
   alias cmake='/snap/cmake/current/bin/cmake'
   ```
5. The installation of the ESS DAQ requires conan, a C++ package
   manager. Conan itself requires Python. However, with Python2.X the
   installation works not correctly. Thus, remove the Python2 packages
   from your system via
   ```bash
   sudo apt remove python2*
   ```
   Add then the following alias to your ``.bashrc``:
   ```bash
   alias python='python3'
   alias pip='pip3'
   ```
6. Install the Slow Control software, following the instructions given
   in [here](https://gitlab.cern.ch/rd51-slow-control/vmmsc/).
   Just as a reminder: if you run into problems during the
   installation, please check the troubleshooting page.
7. Before installing the ESS DAQ, make sure that you have the correct
   version. Please revert to a certain commit via
   ```bash
   git reset --hard a5cfbfe5823e74eda9bf5ed82c6e113b25537763
   ```
   inside of ``/home/USERNAME/essdaq/``
8. Execute the ``install.sh`` script in the essdaq folder.
   State yes all the time and ignore the error messages.
9. Some parts of the ESS DAQ have not been build, so now they are
   being build manually. First, start with the Event Formation Unit
   (EFU).<br>
      a. Go the ``essdaq/efu/event-formation-unit/build`` directory<br>
      b. Execute ``cmake ..``<br>
      c. You might get some errors. All these errors can be fixed. As the
         error type may vary, all common ones are listed in the
         troubleshooting part of this documentation.<br>
      d. After you did the troubleshooting to the corresponding error,
         try ``cmake ..`` in the
         ``essdaq/efu/event-formation-unit/build`` directory.<br>
      e. You might run into another error. Repeat this procedure, until
         no errors occur anymore.<br>
      f. Then execute ``make``.
10. Now continue with the DAQuiri part of the ESS DAQ.
    Go to ``essdaq/daquiri/daquiri/build`` and execute ``cmake ..``
    and ``make``. Follow the same procedure as for the EFU.
11. Now install ``ROOT``. See https://root.cern/install/ on how to
    install it. It is recommended to install the precompiled binary
    for Ubuntu 18.04, version ``6.24/06``:
    https://root.cern/releases/release-62406/.
    Do not forget to install the ROOT prequesites (already adjusted
    for vmm-sdat):
    ```bash
    sudo apt-get install dpkg-dev binutils libx11-dev libxpm-dev libxft-dev libxext-dev libssl-dev
    ```
12. Install the cluster reconstruction software ``vmm-sdat``.
    Just go in the corresponding directory, create the build-directory
    and just execute ``cmake ..`` and ``make``.
13. The installation should be successfully finished. Now continue
    with the set-up and configuration procedure.

#### Legacy configuration procedure

In the following the configuration is shown, to make the software running

* **ESS DAQ config script**:
  follow the instructions given [here](https://github.com/ess-dmsc/essdaq/blob/master/README.md) (only point 1 and 2 from there). <br>
  _If_ you need to create your own detector category, follow also point 3 from there. Otherwise use the existing **GdGEM** class. <br>
  A tested example of the system configuration script is provided [here](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/software-installation/system_GDDLAB.sh), in this folder. Please ensure to adapt the network port name (here _ens8f0_)
  and the name of the EFU calibration and EFU config files (here _GDDLAB_) to your needs.
* The _EFU calibration_ file is the one, which is generated by the calibration module of the Slow Control. Just rename it to GDDLAB_calib.json
  (replace GDDLAB with the name that you chose).
* The _EFU configuration_ file describes the detector geometry, the electronics settings and the clustering conditions for the EFU.
  It is also required for a correct display of the data in DAQuiri. <br>
  An example is provided [here](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/tree/master/software-installation/GDDLAB_config.json). It shows a setup with 2 FECs
  and 2 detectors with x-y-strip readout with 256 strips in each direction.<br>
  The hybrids of the first detector are connected to the FEC 1, while the hybrids of detector 2 are connected to FEC 2.<br>
  The hybrids on FEC 1 are connected to the DVMM ports 5, 6, 7 and 8, with 5 and 6 reading out the x-plane and 7 and 8 reading out the y-plane.<br>
  The hybrids on FEC 2 are connected to the DVMM ports 1, 2, 6 and 7, with 1 and 2 reading out the x-plane and 6 and 7 reading out the y-plane.<br>
  !!! note "Data display in DAQuiri"

      To display the data in DAQuiri from the GdGEM module of the EFU, use the NMX profile of DAQuiri. It shows only one detector.
      To visualise more than one detector, increase the offset in the configuration file.
      In the given example, the coordinates of the second detector do not start in x = 0 and y = 0 but in x = 256 and y = 256.

!!! danger "Network port"

    The network port, where you are connecting the SRS to, should have
    the IP address ``10.0.0.3`` with a netmask of ``255.255.255.0``
    and an MTU of 9000.
    Please ensure that you have the things set like this.
    It is one of the most common mistakes!
