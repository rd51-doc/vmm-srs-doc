# Software installation

In the following a brief overview on the software installation procedure is given.
For troubleshooting, see the troubleshooting directory.

!!! warning "Installation order"

    It should be noted that the installation order and the correct
    fulfillment of the needed software requirements are important.
    This means:

    * Do not install the software components in a different order then
      the one described here. You might miss some dependencies.
    * The installation procedure was only tested on Ubuntu 18.04 LTS.
      If you try any other operating system or version, you might run
      into problems, which are not part of this documentation.
    * When you change your ``.bashrc`` file, do not forget to
      ``source`` it or just restart your computer.
    * It is recommended to read through the entire installation
      procedure AND the troubleshooting part **before** actually
      starting the installation.

#### Installation of dependencies

1. **Install git, python3 and make:** make sure that ``git``, ``python3`` and ``make`` are installed. If they are not installed, install them via
   ```bash
   sudo apt install git make python3
   ```
2. **Install CMake:** install ``cmake`` from the Ubuntu software centre (snap package). The installation was successfully tested with version ``3.21.4`` of ``cmake``.
   Create an alias in the ``.bashrc`` for cmake:
   ```bash
   alias cmake='/snap/cmake/current/bin/cmake'
   ```
3. **Install conan:** to install ``vmm-sdat`` conan, a C++ package manager, is needed. Conan itself requires Python. However, with Python2.X the installation works not correctly. Thus, remove the Python2 packages from your system via
   ```bash
   sudo apt remove python2*
   ```
   Add then the following alias to your ``.bashrc``:
   ```bash
   alias python='python3'
   alias pip='pip3'
   ```
   Then install conan via
   ```bash
   pip3 install -v "conan==1.59.0"
   ```
4. **Install Qt:** if you do not have Qt on your system, download it from the [Qt pages](https://download.qt.io/official_releases/qt/5.12/5.12.9/). 
   The installer for Qt version 5.12.9 weights about 1.3GB. The full installation procedure of Qt takes less than 1 hour on a machine with adequate internet connection.
   Once downloaded, make sure the file is executable:
   ```bash
   chmod +x qt-opensource-linux-x64-5.12.9.run
   ```
   Now, install. The installation folder should be "/home/username/Qt5.12.9/". For this case, username is "daq", so the installation folder is "/home/daq/Qt5.12.9/".
   ```bash
   sudo ./qt-opensource-linux-x64-5.12.9.run
   ```
   Follow the installer prompts. Make sure to select installation of all components options. Once the installer is finished, it is highly recommended to remove administrator rights for the Qt directory and all subdirectories, so the sudo command does not need to be applied every time Slow Control is run.
   ```bash
   sudo chmod -R 777 /home/daq/Qt5.12.9/
   ```
   To check that the installation has been successful:
   ```bash
   qmake --version
   ```
5. **Install OpenGL:** install OpenGL via
   ```bash
   sudo apt-get install libglu1-mesa-dev freeglut3-dev mesa-common-dev
   ```
6. **Install gcc-9:** please make sure that at least ``gcc/g++`` version 9 is installed. Just type
   ```bash
   gcc --version
   ```
   If it is version 9 or higher, just skip the next instructions and jump to point 2.
   Otherwise, ddd the Ubuntu Toolchain test repository list:
   ```bash
   sudo add-apt-repository ppa:ubuntu-toolchain-r/test
   sudo apt update
   sudo apt install gcc-9
   sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 9
   sudo apt install g++-9
   sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 9
   ```

#### Installation of components

1. **Wireshark:** an essential tool for operating VMM3a/SRS, which should be installed before the Slow Control is Wireshark. Please install Wireshark with Lua support.
   To install Wireshark type
   ```bash
   sudo apt install wireshark-qt
   ```
   In [this repository](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/blob/large_systems/vmm3a_srs.lua), a lua script is provided that turns HEX-values of the UDP packages from the FEC, displays the hits from the VMMs and makes the information human readable.
   It might be convenient to create an alias in ```.bashrc`` to start Wireshark with the corresponding Lua script:
   ```bash
   alias essws='wireshark -X lua_script:/path_to_slow_control/vmmsc/vmm3a_srs.lua'
   ```
   Please add Wireshark to the sudoers group via
   ```bash
   sudo adduser $USER wireshark
   ```
2. Perform the ``git clone`` commands of the software
   ```bash
   git clone https://gitlab.cern.ch/rd51-slow-control/vmmsc
   git clone https://github.com/ess-dmsc/vmm-sdat
   ```
3. **Slow Control:**
   Some modifications have to be added to the .bashrc:
   ```bash
   export PATH="/home/daq/Qt5.12.9/5.12.9/gcc_64/bin/:$PATH"
   export CXX="/usr/bin/g++-9"
   ```
   Make sure to reload the bashrc via
   ```bash
   source .bashrc
   ```
   Now navigate to the new directory (assuming that the starting point is the home directory):
   ```bash
   cd vmmsc/build
   ```
   Run ``qmake``. Make sure you use the correct qmake in case you have several Qt versions installed:
   ```bash
   qmake vmmdcs.pro
   ```
   The next step is to compile the package:
   ```bash
   make clean
   make -j10
   ```
   The executable will now be located in the path ``vmmsc/build``
   To open Slow Control on linux use the following command:
   ```bash
   ./vmmdcs
   ```
   On macOS use this command:
   ```bash
   ./vmmdcs.app/Contents/MacOS/vmmdcs
   ```
   The ``vmmdcs.pro`` file is set up to point to the include paths/dependencies based on whether the operating system is linux or macOS (mac being the only other considered system other than linux).
   Just as a reminder: if you run into problems during the installation, please check the troubleshooting page.
4. If not already done before, install ``ROOT``. See https://root.cern/install/ on how to install it.
    Do not forget to install the ROOT prequesites (already adjusted for ``vmm-sdat``):
    ```bash
    sudo apt-get install dpkg-dev binutils libx11-dev libxpm-dev libxft-dev libxext-dev libssl-dev
    ```
5. **Install ``vmm-sdat``:**
   Just go in the corresponding directory, create the build-directory and just execute ``cmake ..`` and ``make``, as described [here](https://github.com/ess-dmsc/vmm-sdat).

#### Configuration procedure

!!! danger "Network port"

    The network port, where you are connecting the SRS to, should have
    the IP address ``10.0.0.3`` with a netmask of ``255.255.255.0``
    and an MTU of 9000.
    Please ensure that you have the things set like this.
    It is one of the most common mistakes!
