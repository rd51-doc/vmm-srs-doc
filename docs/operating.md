# Operating

#### Operating the system
To see, how to operate the system, please have a look at the tutorial
video, mentioned above, as well as the
[system operation](./operating/overview.md)
section of this guide.
Within this section, please pay particular attention to the
[*VMM and SRS - a guide for dummies*](./operating/vmm-and-srs-infn-rome.pdf).
