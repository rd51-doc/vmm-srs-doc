# Troubleshooting---Slow Control

#### qt.qpa.plugin
When you try to start the slow control and have an error message similar to this one
```bash
qt.qpa.plugin: Could not find the Qt platform plugin "xcb" in ""
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem
```
try the following:

1. add
   ```bash
   export QT_PLUGIN_PATH=/home/USERNAME/Qt5.12.9/5.12.9/gcc_64/plugins
   ```
   to your ``.bashrc`` and reinstall the Slow Control via
   ``make clean``, ``qmake vmmdcs.pro`` and ``make`` in the build-directory
   of the Slow Control folder.
2. alternatively, it may also be an error in your ``$DISPLAY`` environment variable.
   It should be set to your network IP, with ``:0`` at the end.
   For instance, if your IP is ``10.0.0.5``, you should write: ``export DISPLAY=10.0.0.5:0``
   You can try it by running xeyes or any QT GUI app.
   You can also automatise it by adding this to your ``.bashrc``:
   ```bash
   export DISPLAY=$(ip route list default | awk '{print $3}'):0
   ```

#### Slow control is not working
* Remove firewall blocking of UDP
* An MTU value of 9000 (jumbo frames) is needed in the network card settings
* Check the firmware combination
