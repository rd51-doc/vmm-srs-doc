# Troubleshooting---Conan


#### ERROR: HTTPSConnectionPool(host='artifactoryconan.esss.dk', port=443): Max retries exceeded
If you get an error like this
```bash
fmt/6.2.0: Package is up to date
ERROR: HTTPSConnectionPool(host='artifactoryconan.esss.dk', port=443): Max retries exceeded with url: /artifactory/api/conan/ecdc/v1/ping (Caused by ResponseError('too many 503 error responses')). [Remote: ecdc]

CMake Error at cmake/modules/conan.cmake:371 (message):
  Conan install failed='1'
Call Stack (most recent call first):
  cmake/modules/conan.cmake:451 (conan_cmake_install)
  CMakeLists.txt:26 (conan_cmake_run)


-- Configuring incomplete, errors occurred!
See also "/home/daq/vmm-sdat/build/CMakeFiles/CMakeOutput.log".
```
please try to run this command
```bash
conan config install https://github.com/conan-io/conanclientcert.git
```
as stated also [here](https://stackoverflow.com/questions/69719363/httpsconnectionpool-error-when-trying-to-install-gtest-with-conan).


#### CONAN_REVISIONS_ENABLED=1
If you have an error message similar to this one:
```bash
ERROR: The remote at 'https://bincrafters.jfrog.io/artifactory/api/conan/public-conan' only works with revisions enabled.
Set CONAN_REVISIONS_ENABLED=1 or set 'general.revisions_enabled = 1' at the 'conan.conf'. [Remote: bincrafters]
```
Please add ``revisions_enabled = 1`` underneath of ``[general]`` in
``conan.conf`` in the ``.conan`` directory inside of your home directory.


#### Compiler version not stated
You might get a conan-related error, which states that the compiler
and the compiler version are not stated. To solve this, go to the
``settings.yml`` file in the ``.conan`` directory and add
```yml
compiler=gcc
compiler.version=9.X
```
with ``X`` being 3 or 4 (check via ``g++ --version``).


#### Conan compiler error (1)
If you get an error like this
```bash
CMake Deprecation Warning at CMakeLists.txt:1 (cmake_minimum_required):
Compatibility with CMake < 2.8.12 will be removed from a future version of
CMake.

Update the VERSION argument <min> value or use a ...<max> suffix to tell
CMake that the project does not need compatibility with older versions.

-- Conan: Automatic detection of conan settings from cmake
-- Conan: Settings= -pr;default
-- Conan executing: conan install /home/dateuser/VMM/essdaq/efu/event-formation-unit/conanfile.txt -pr default -g=cmake --build=outdated --no-imports
-- ERROR: compiler not defined for compiler.libcxx
Please define compiler value first too

CMake Error at cmake/modules/conan.cmake:371 (message):
  Conan install failed='1'
Call Stack (most recent call first):
  cmake/modules/conan.cmake:451 (conan_cmake_install)
  CMakeLists.txt:37 (conan_cmake_run)

-- Configuring incomplete, errors occurred!
See also "/home/dateuser/VMM/essdaq/efu/event-formation-unit/build/CMakeFiles/CMakeOutput.log".
```
add
```bash
compiler=gcc
compiler.version=9.4
```
in ``~/.conan/profiles/default`` BEFORE ``compiler.libcxx=libstdc++11``
and reboot!


#### Conan compiler error (2)
If you get an error like this
```bash
CMake Deprecation Warning at CMakeLists.txt:1 (cmake_minimum_required):
Compatibility with CMake < 2.8.12 will be removed from a future version of
CMake.

Update the VERSION argument <min> value or use a ...<max> suffix to tell
CMake that the project does not need compatibility with older versions.

-- The C compiler identification is GNU 9.4.0
-- The CXX compiler identification is GNU 9.4.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/g++-9 - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found Git: /usr/bin/git (found version "2.17.1")
-- Conan: Automatic detection of conan settings from cmake
-- Conan: Settings= -pr;default
-- Conan executing: conan install /home/dateuser/VMM/essdaq/efu/event-formation-unit/conanfile.txt -pr default -g=cmake --build=outdated --no-imports
-- ERROR: Invalid setting '9.4' is not a valid 'settings.compiler.version' value.
Possible values are ['4.1', '4.4', '4.5', '4.6', '4.7', '4.8', '4.9', '5', '5.1', '5.2', '5.3', '5.4', '5.5', '6', '6.1', '6.2', '6.3', '6.4', '6.5', '7', '7.1', '7.2', '7.3', '7.4', '7.5', '8', '8.1', '8.2', '8.3', '8.4', '9', '9.1', '9.2', '9.3', '10', '10.1']
Read "http://docs.conan.io/en/latest/faq/troubleshooting.html#error-invalid-setting"

CMake Error at cmake/modules/conan.cmake:371 (message):
  Conan install failed='1'
Call Stack (most recent call first):
  cmake/modules/conan.cmake:451 (conan_cmake_install)
  CMakeLists.txt:37 (conan_cmake_run)

-- Configuring incomplete, errors occurred!
See also "/home/dateuser/VMM/essdaq/efu/event-formation-unit/build/CMakeFiles/CMakeOutput.log".
```
edit ``.conan/settings.yml`` and add ``9.4``, to the gcc versions:
```bash
compiler:
  sun-cc:
      version: ["5.10", "5.11", "5.12", "5.13", "5.14", "5.15"]
      threads: [None, posix]
      libcxx: [libCstd, libstdcxx, libstlport, libstdc++]
  gcc: &gcc
      version: ["4.1", "4.4", "4.5", "4.6", "4.7", "4.8", "4.9",
                "5", "5.1", "5.2", "5.3", "5.4", "5.5",
                "6", "6.1", "6.2", "6.3", "6.4", "6.5",
                "7", "7.1", "7.2", "7.3", "7.4", "7.5",
                "8", "8.1", "8.2", "8.3", "8.4",
                "9", "9.1", "9.2", "9.3", "9.4",
                "10", "10.1"]
```


#### WARN: Can't find a [...] package for the specified settings, options and dependencies:
In case you get an error while installing a conan package, which looks like this
```bash
dateuser@MiniHP-VMM:~/.conan/data$ conan install benchmark/1.5.0@
Configuration:
[settings]
arch=x86_64
arch_build=x86_64
build_type=Release
compiler=gcc
compiler.libcxx=libstdc++11
compiler.version=9.4
os=Linux
os_build=Linux
[options]
[build_requires]
[env]

benchmark/1.5.0: Not found in local cache, looking in remotes...
benchmark/1.5.0: Trying with 'conancenter'...
Downloading conanmanifest.txt completed [0.17k]                                          
Downloading conanfile.py completed [3.69k]                                               
Downloading conan_export.tgz completed [0.24k]                                           
Decompressing conan_export.tgz completed [0.00k]                                         
benchmark/1.5.0: Downloaded recipe revision 8f9fda5c5a906b4f3f2f10ca68922ecf
Installing package: benchmark/1.5.0
Requirements
    benchmark/1.5.0 from 'conancenter' - Downloaded
Packages
    benchmark/1.5.0:2ce7dd302c24e6ab9c4dfc02348bacb2759d092a - Missing

Installing (downloading, building) binaries...
ERROR: Missing binary: benchmark/1.5.0:2ce7dd302c24e6ab9c4dfc02348bacb2759d092a

benchmark/1.5.0: WARN: Can't find a 'benchmark/1.5.0' package for the specified settings, options and dependencies:
- Settings: arch=x86_64, build_type=Release, compiler=gcc, compiler.libcxx=libstdc++11, compiler.version=9.4, os=Linux
- Options: enable_exceptions=True, enable_lto=False, fPIC=True, shared=False
- Dependencies:
- Requirements:
- Package ID: 2ce7dd302c24e6ab9c4dfc02348bacb2759d092a

ERROR: Missing prebuilt package for 'benchmark/1.5.0'
Try to build from sources with '--build=benchmark'
Use 'conan search <reference> --table table.html'
Or read 'http://docs.conan.io/en/latest/faq/troubleshooting.html#error-missing-prebuilt-package'
```

or like this

```bash
dateuser@MiniHP-VMM:~/.conan/data$ conan install fmt/6.2.0@
Configuration:
[settings]
arch=x86_64
arch_build=x86_64
build_type=Release
compiler=gcc
compiler.libcxx=libstdc++11
compiler.version=9.4
os=Linux
os_build=Linux
[options]
[build_requires]
[env]

fmt/6.2.0: Not found in local cache, looking in remotes...
fmt/6.2.0: Trying with 'conancenter'...
Downloading conanmanifest.txt completed [0.42k]                                          
Downloading conanfile.py completed [4.33k]                                               
Downloading conan_export.tgz completed [0.24k]                                           
Decompressing conan_export.tgz completed [0.00k]                                         
fmt/6.2.0: Downloaded recipe revision 6e58c9a85c09b9b672d5adbc4734125d
Installing package: fmt/6.2.0
Requirements
    fmt/6.2.0 from 'conancenter' - Downloaded
Packages
    fmt/6.2.0:2558d3f530bd573703a3a178886bc65bf0814523 - Missing

Installing (downloading, building) binaries...
ERROR: Missing binary: fmt/6.2.0:2558d3f530bd573703a3a178886bc65bf0814523

fmt/6.2.0: WARN: Can't find a 'fmt/6.2.0' package for the specified settings, options and dependencies:
- Settings: arch=x86_64, build_type=Release, compiler=gcc, compiler.libcxx=libstdc++11, compiler.version=9.4, os=Linux
- Options: fPIC=True, header_only=False, shared=False, with_fmt_alias=False
- Dependencies:
- Requirements:
- Package ID: 2558d3f530bd573703a3a178886bc65bf0814523

ERROR: Missing prebuilt package for 'fmt/6.2.0'
Try to build from sources with '--build=fmt'
Use 'conan search <reference> --table table.html'
Or read 'http://docs.conan.io/en/latest/faq/troubleshooting.html#error-missing-prebuilt-package'
```

do the following: go to the ``.conan/data`` directory and try to install the package like this

```bash
conan install benchmark/1.5.0@ --build
conan install fmt/6.2.0@ --build
```


#### ERROR: 404: Not found.
If you have an error message similar to this one:
```bash
ERROR: 404: Not Found. [Remote: conancenter]
The 'benchmark/1.5.0' package has 'exports_sources' but sources not found in local cache.
Probably it was installed from a remote that is no longer available.
CMake Error at cmake/modules/conan.cmake:371 (message):
Conan install failed='1'
Call Stack (most recent call first):
cmake/modules/conan.cmake:451 (conan_cmake_install)
CMakeLists.txt:37 (conan_cmake_run)
```
Please do the following:
go to the ``.conan/data`` directory inside of your home directory.
Execute
```bash
conan remove benchmark
conan install benchmark/1.5.0@
```
This error can occur with any conan package. It can be solved in the
fashion. For example
```bash
conan remove fmt
conan install fmt/6.2.0@
```
