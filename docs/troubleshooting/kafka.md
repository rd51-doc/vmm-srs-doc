# Troubleshooting---KAFKA

#### Building the EFU and kafka
Once ``cmake ..`` of the EFU was successful and you try to build it
via ``make`` and get an error containing something like this
```bash
rdkafka_msgset_writer.c:(.text+0x4fb1): undefined reference to `mtx_lock'
rdkafka_msgset_writer.c:(.text+0x4ffb): undefined reference to `mtx_unlock'
rdkafka_msgset_writer.c:(.text+0x511a): undefined reference to `mtx_lock'
rdkafka_msgset_writer.c:(.text+0x516b): undefined reference to `mtx_unlock'
```
do the following:
```bash
sudo apt install librdkafka
sudo apt install librdkafka-dev
cd ~/.conan/data
conan install librdkafka/1.6.0@ --build
```
Then, go back to the EFU-build-directory, execute ``cmake ..`` again,
and try to build the EFU again via ``make -j4``.

#### No local Kafka found (1)
When you start Kafka or check if Kafka is running (``essdaq/kafka/start_kafka.sh`` or ``essdaq/kafka/check_kafka.sh``), this error might occur:
```bash
~/essdaq/kafka ~/essdaq/kafka
./check_kafka.sh: line 7: ../config/system.sh: No such file or directory
Checking Kafka at ip address
usage: check_kafka.py [-h] bootstrap_servers
check_kafka.py: error: the following arguments are required: bootstrap_servers
checking for local kafka instance
./check_kafka.sh: line 14: netstat: command not found
No local Kafka found
```
This means that the ``system.sh`` file has not been created as described in the software configuration section of this document.

#### No local Kafak found (2)
If you get an error related to Kafka like this:
```bash
Checking Kafka at ip address 127.0.0.1
Brokers found
checking for local kafka instance
./check_kafka.sh: line 14: netstat: command not found
No local Kafka found
```
Please install the net-tools: ``sudo apt install net-tools``

#### No local Kafak found (3)
``kafka_check.sh`` might say that ``No local Kafka found``, after execute ``kafka_start.sh`` without error message.
Wait 5 seconds and check again. If Kafka is still not found, try ``kafka_start.sh`` again, wait and check again.
Normally, Kafka should run now.
If Kafka still is not running, do the following: go to ``essdaq/kafka/kafka/logs/`` and remove the content of the entire folder.
Then try again to start Kafka and check again.

