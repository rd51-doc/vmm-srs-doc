# Troubleshooting---Other

#### Wireshark does not display any network interface
Wireshark might not display any network interface: make sure that wireshark is part of the sudoers group.
To add wireshark to the sudeors group, type ``sudo adduser $USER wireshark`` in the command line.
*Afterwards reboot*!

#### Hardware problems
In case there should be indeed some hardware problems, the
reader may check out this online folder, where all the documentation for
the hardware and possible fixes (also for SRS parts that are non-VMM-related)
can be found:
[Hans Muller's Google Drive](https://drive.google.com/drive/folders/1h5KLMAa7-bxbPoisnip5Adb4mYc2YMdq)

As a general rule, it is advised not to modify the hardware without consultation of Hans Muller!

#### DAQ destination IP `0.0.0.0` -> HDMI not found
* Check output of iMPACT (the log that is written into the bottom window)
* Was the flash programmed?

#### No ping
* Check the SFP adapter. It must be of equivalent to 1000BASE-T 1.25 GBd 3.3V
  like ABCU-5730GZ (but not ABCU-5731GZ which is the SGMII version)
* Check the jumpers on the FEC: [FECv6 jumpers](https://drive.google.com/file/d/1MEyOzR1zJt-uHTDvpocF-teJQje0TGCv/view?usp=sharing)
* Check the crate power on the green FEC power connector:
  [Powercrate](https://drive.google.com/file/d/1QElf_vUhjJCs6nB3YP7rbKI9PFB9p_R5/view?usp=sharing)

#### Compiler error with vmm-sdat
* There is one known error with a huge amount of error messages being shown, potentially pointing to some ROOT functions.
  This is not necessarily related to ROOT, but more to the C++ standard defined in the CMake file.
  For Ubuntu systems it seems that C++14 is the working one, while on macOS it seems to be C++17.
  So once you get this large amount of error messages at once, try to change the C++ standard in the CMake file.
* Another error that was found is the following
  ```bash
  /usr/bin/ld: libvmm-sdat.so: undefined reference to `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > fmt::v6::internal::grouping_impl<char>(fmt::v6::internal::locale_ref)'
  collect2: error: ld returned 1 exit status
  ```
  As you can read from the message, it is related to the fmt package installed by conan and needed for vmm-sdat.
  To solve this particular issue, you have to add ``#define FMT_HEADER_ONLY`` above ``#include <fmt/format.h>`` in the source files that use fmt, which are for the moment
  ```
  src/parser/SRSTime.cpp
  src/parser/ReaderPcap.cpp
  src/parser/Trace.h
  ```