# Troubleshooting---DAQUIRI

#### QMutex error
If you run into the following ``QMutex``-related error,
while building DAQuiri
```bash
Error:  In file included from /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:8:
/home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.h:36:3: error: ‘QMutex’ does not name a type
  36 |   QMutex mutex;
     |   ^~~~~~
/home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp: In member function ‘virtual void WorkerThread::run()’:
/home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:23:25: warning: comparison of integer expressions of different signedness: ‘std::chrono::duration<long int, std::ratio<1, 1000000000> >::rep’ {aka ‘long int’} and ‘long long unsigned int’ [-Wsign-compare]
  23 |     if (elapsed.count() >= 1000000000ULL) {
     |         ~~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~
/home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:24:7: error: ‘mutex’ was not declared in this scope; did you mean ‘std::mutex’?
  24 |       mutex.lock();
     |       ^~~~~
     |       std::mutex
In file included from /usr/include/c++/9/mutex:43,
                 from /usr/include/c++/9/future:38,
                 from /home/meddaq/Qt-5.12.9/5.12.9/gcc_64/include/QtCore/qthread.h:50,
                 from /home/meddaq/Qt-5.12.9/5.12.9/gcc_64/include/QtCore/QThread:1,
                 from /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.h:16,
                 from /home/meddaq/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.cpp:8:
/usr/include/c++/9/bits/std_mutex.h:83:9: note: ‘std::mutex’ declared here
  83 |   class mutex : private __mutex_base
     |         ^~~~~
source/daqlite/CMakeFiles/daqlite.dir/build.make:173: recipe for target 'source/daqlite/CMakeFiles/daqlite.dir/WorkerThread.cpp.o' failed
make[2]: *** [source/daqlite/CMakeFiles/daqlite.dir/WorkerThread.cpp.o] Error 1
CMakeFiles/Makefile2:795: recipe for target 'source/daqlite/CMakeFiles/daqlite.dir/all' failed
make[1]: *** [source/daqlite/CMakeFiles/daqlite.dir/all] Error 2
Makefile:90: recipe for target 'all' failed
make: *** [all] Error 2
```
just add ``#include <QMutex>`` to
``/home/USERNAME/essdaq/daquiri/daquiri/source/daqlite/WorkerThread.h``

#### DAQuiri does not display anythin
It might happen that DAQuiri does not display anything despite the correct settings of all components.
It could be that there is some problem with the NMX profile files.
So rename the existing profile files (``default_consumers.daq`` and ``profile.set`` in ``essdaq/daquiri/profiles/nmx``) and try to operate the
system with the files, which are provided in this folder here: [``default_consumers.daq``](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/blob/master/troubleshooting/default_consumers.daq) and [``profile.set``](https://gitlab.cern.ch/rd51-slow-control/vmm-doc/-/blob/master/troubleshooting/profile.set).
It might be useful to reboot the computer before operating with the new profile files.

#### DAQuiri does not display anything
When you start DAQUiri and it does not display anything and in the terminal somethin like this is written
```bash
dateuser@MiniHP-VMM:~/VMM/essdaq/detectors/gdgem$ ./daquiri.sh
~/VMM/essdaq/daquiri/daquiri/build ~/VMM/essdaq/detectors/gdgem
[2021-12-02 17:52:36.261251] [info] [processID: 9928]: <Engine> Initialized profile ""
[2021-12-02 17:53:15.210481] [info] [processID: 9928]: <Engine> Initialized profile "NMX"
[2021-12-02 17:53:15.210537] [info] [processID: 9928]: <ThreadRunner::run()> - action: Boot
<Engine::boot>
[2021-12-02 17:53:15.231560] [info] [processID: 9928]: <ESSStream:nmx_detector> booted with consumer rdkafka#consumer-1
[2021-12-02 17:53:15.231865] [info] [processID: 9928]: <ESSStream:nmx_monitor> booted with consumer rdkafka#consumer-2
[2021-12-02 17:53:16.232793] [info] [processID: 9928]: <ThreadRunner::run()> - action: Acquire
[2021-12-02 17:53:16.234406] [info] [processID: 9928]: <Engine> Starting acquisition for indefinite run
[2021-12-02 17:53:16.235760] [info] [processID: 9928]: <ESSStream:nmx_detector> Starting run, timeout: 1000
[2021-12-02 17:53:16.235941] [info] [processID: 9928]: <ESSStream:nmx_monitor> Starting run, timeout: 1000
[2021-12-02 17:53:16.248803] [warning] [processID: 9928]: <ESSStream> nmx_detector:0 Consume failed! Err=Subscribed topic not available: nmx_detector: Broker: Unknown topic or partition
[2021-12-02 17:53:16.251462] [warning] [processID: 9928]: <ESSStream> nmx_monitor:0 Consume failed! Err=Subscribed topic not available: nmx_monitor: Broker: Unknown topic or partition
[2021-12-02 17:53:21.242931] [info] [processID: 9928]:   RUNNING Elapsed: 0:00:05  Dropped spills: 0  Accepted events: 0  Dropped events: 0
[2021-12-02 17:53:26.249695] [info] [processID: 9928]:   RUNNING Elapsed: 0:00:10  Dropped spills: 0  Accepted events: 0  Dropped events: 0
[2021-12-02 17:53:31.256948] [info] [processID: 9928]:   RUNNING Elapsed: 0:00:15  Dropped spills: 0  Accepted events: 0  Dropped events: 0
```
please make sure that the output contains really one of these statements:
```bash
<ESSStream> nmx_monitor:0 Consume failed! Err=Subscribed topic not available: nmx_monitor: Broker: Unknown topic or partition
```
If this is the case, do the following: Go to the file ``essdaq/efu/event-formation-unit/src/modules/gdgem/GdGemBase.cpp`` and replace
in line 400 to 402 ``nmx_`` with ``NMX_``.
The capitalisation is important!

Then do not forget to rebuild the EFU! For this you just have to go to ``essdaq/efu/event-formation-unit/build`` and type ``make``.


#### DAQuiri still does not show anything
If DAQUiri still does not show anything: make sure that the MTU of all physical network ports of the DAQ computer are set to 9000!
If setting the MTU of one of the network ports to 9000 does not work, do the following (not recommended!): add ``--nohwcheck`` to the
EFU start script:

```bash
#!/bin/bash

echo "START GDGEM"

# change to directory of script
cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null
export DETECTORDIR=$(pwd)

source ../../config/scripts/base.sh

systemChecks

#
# #
#

CALIBARG=""
if [[ $EFU_CALIB != "" ]]; then
  CALIBARG="--calibration $(pwd)/${EFU_CONFIG}_calib.json"
fi

CONFIGARG=""
if [[ $EFU_CONFIG != "" ]]; then
  CONFIGARG="--file $(pwd)/${EFU_CONFIG}_config.json"
fi

startDaquiri

../../efu/efu_start.sh  $CONFIGARG $CALIBARG $@ --nohwcheck
```

#### DAQuiri still does not show anything
If DAQUiri still does not show anything: go to ``essdaq/daquiri/daquiri``
execute ``git reset --hard 9628d2eb2423a089070cd956cac5ef4666d0e0b4`` then go to the ``build`` directory
and then execute ``cmake ..`` and ``make``.

#### DAQuiri does not display data from an entire FEC (only in multi-FEC systems)
* DO NOT try to open communication -> the ping might not work and the Slow Control falls into 'ping failed' state
* Possible recovery: ACQ Off -> Warm Init FEC -> ACQ On
* If Daquiri still does not show data for the FEC, restart the EFU
* If the FEC is still missing, perform a powercycle