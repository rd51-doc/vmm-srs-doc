# Introduction

With this document a little guidance through the first steps of
using VMM3a/SRS should be provided, allowing to successfully operate
the system.

For information on which electronics settings to use, have a look
[here](https://cds.cern.ch/record/2860765/).
Or just play with the system and optimise it according to your experimental needs :-)

!!! note "Tutorial video"

    In case you are wondering, how to operate the system, there
    is a [tutorial video](https://indico.cern.ch/event/1071632/contributions/4615369/attachments/2345888/4000941/VMM-SRS-SoftwareInstallation-Scharenberg.mp4)
    available.
    
    It is already a bit older and the components have been updated since then.
    Some of the shown features, like online monitoring via EFU and Daquiri have been
    put to legacy status and been replace with a more simple to use approach
    (within [vmm-sdat](https://github.com/ess-dmsc/vmm-sdat)).
    Nonetheless, the video provides a rough overview on how the system functions.
    So if you are a new user, it is still recommended to watch the video before going ahead
    with turning on the system.

    Even though some background information is provided in the video,
    it assumes that you are roughly familiar with the system, i.e.
    what the SRS is, the components of VMM3a/SRS, etc.

!!! note "Please read the entire guide and use the search function"

    There are many reoccurring questions on the system.
    Most of them have been answered in this guide.
    Please keep in mind that some of these answers might not be were you expect them.
    So please check out all categories and use the search function (top bar of this website).

One comment to the reader before the start: please keep in mind that
VMM3a/SRS is a powerful system but also a system which is further
developed and improved.
So not everything may run as smooth as you may think/hope/wish.
Therefore, the contribution of the reader to the development of the
system and to help to create a mature readout system for particle
detectors is essential.

The same applies to this document.
It is _always_ work in progress and changes accordingly to the
developments on the system.

For changes and suggestions to improve this document, it is
recommended to use the standard `git` tools, so pull requests (GitHub)
or merge requests (GitLab).
_Or just create an issue on the [CERN GitLab webpage of this document](https://gitlab.cern.ch/rd51-doc/vmm-srs-doc)_.

As we are here working with GitLab and in case you might not know how
to use `git` and how to do merge requests, please find some information
under the following links (last accessed 27 April 2021):

* [CERN Git Service](https://information-technology.web.cern.ch/services/git-service)
* [Git Documentation](https://git-scm.com/doc)
* [GitLab Basics](https://docs.gitlab.com/ee/gitlab-basics/)
* [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/)
* [Merge Requests Getting Started](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html)
* [Creating Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)


#### Contacts

In case of persistent problems, join the discussion in one of the
corresponding channels of [Discord](https://discord.gg/jsT9HgD) and
post it there.
Maybe your question was also already asked there, so please check this
before posting.

If you want to contact one of the developers directly, please check
out the README files on the GitHub or GitLab pages of the
software/firmware projects.
Usually the contact details are mentioned there.

If you have a very specific problem, which could not be solved with
this document or the above hints you can contact the following people:

* Electronics in general, hardware: [Hans Muller](mailto:hans.muller@cern.ch), [Jochen Kaminski](mailto:kaminski@physik.uni-bonn.de)
* General questions, system operation: [Lucian Scharenberg](mailto:lucian.scharenberg@cern.ch), [Dorothea Pfeiffer](mailto:dorothea.pfeiffer@cern.ch), [Michael Lupberger](mailto:michael.lupberger@cern.ch)
* Software: [Dorothea Pfeiffer](mailto:dorothea.pfeiffer@cern.ch), [Lucian Scharenberg](mailto:lucian.scharenberg@cern.ch), [Michael Lupberger](mailto:michael.lupberger@cern.ch)
* Firmware: [Dorothea Pfeiffer](mailto:dorothea.pfeiffer@cern.ch), [Michael Lupberger](mailto:michael.lupberger@cern.ch)
* Virtual machine and WSL (not officially supported): [Francisco Garcia](mailto:francisco.garcia@cern.ch)
* Discord: [Michael Lupberger](mailto:michael.lupberger@cern.ch)
* SRS Technology: [Alex Rusu](mailto:alex.rusu@srstechnology.ch)
