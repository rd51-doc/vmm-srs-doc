In this section, the different software components are described.

!!! note "Available software"

    The combination of VMM3a and SRS is a new system with its own
    pieces of software.
    The VMM3a is also a new chip, which works differently to previous
    ASICs that have been implemented into the SRS, i.e. the APV25.
    This means that the software for VMM3a/SRS was freshly developed.
    It also means that no old software for APV25/SRS was adapted
    towards VMM3a/SRS.
    It means even further, that the user should (for the beginning)
    ignore all their knowledge about old or other DAQ systems and not
    compare it with these previous systems.

!!! warning "Legacy software"

    In May 2023, some software components were transferred to a
    'legacy status', meaning that there is no support for them anymore.

    For the sake of completeness, the full installation description
    is still kept in the [legacy section](./software/legacy-software.md),
    but with the strong discouragement of using it!
    
!!! danger "Operating system"

    It is **highly** recommended to use Ubuntu 22.04 LTS as operating
    system, running on a physical machine!

    Any other Linux distribution or Ubuntu version was not tested.
    If you want to use a different distribution, you have to test it
    by yourself and not rely on the help of the experts.

    Trying to install the software in a virtual machine, a docker
    image or the WSL has been tested several times, due to the
    interest of the collaborators, but so far no successfully running
    system was set up.
    This means: if you decide to go down this path, the collaboration
    will be certainly interested in your results, but please do not
    expect support from the experts. They recommend to use
    Ubuntu 18.04 LTS on a physical machine.

#### Software components

* **Control software:** RD51 VMM [slow control software](https://gitlab.cern.ch/rd51-slow-control/vmmsc)
* **Raw data monitor:** [Wireshark](https://www.wireshark.org/)
* **Data acquisition for offline analysis:** [tcpdump](https://github.com/the-tcpdump-group/tcpdump)
* **Offline reconstruction and online monitoring:** [vmm-sdat](https://github.com/ess-dmsc/vmm-sdat)


#### Dependencies

* **git:** to get access to most of the software used, the git version control system is needed.
* **Qt:** to use the slow control, Qt has to be installed on the computer. **Version 5.12.9** was found to be working well.
* **ROOT:** the offline reconstruction requires ROOT. **Versions 6.24/06 or 6.28/12** were found to be working well.
* **Python:** the online monitoring requires a working installation of Python3.
* **CMake:** to install vmm-sdat, CMake is needed. **Version 3.21.X** was found to be working well.
* **conan:** to install vmm-sdat, conan is needed. **Please make sure to install version 1.59.X. Version 2.X is not working at the moment.**


#### Network package structure
In the case there is some interest on the structure of the network
packages sent from the FEC to the DAQ computer, in
[these slides](firmware/vmm3a-network.pdf) there is some small (and
not yet fully complete overview given).