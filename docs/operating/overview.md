# VMM3a/SRS Operation

After successfully installing everything, you can now start to
operate your system.

!!! note "Tutorial video"

    Be again reminded on the [tutorial video](https://indico.cern.ch/event/1071632/contributions/4615369/attachments/2345888/4000941/VMM-SRS-SoftwareInstallation-Scharenberg.mp4),
    where it is shown how to operate the system.

The system requires several software components. The operation of
these components is presented in the following.

!!! note "Start up procedure"

    It is useful to follow a certain start up procedure for the
    software components, just to avoid that some component does not
    work, because it is expecting data from another component,
    which has not been started yet.

The recommended start up procedure is as follows (<span style="color:grey">*grey text = legacy software*</span>):

* **Slow control software** to define all the system settings and
  start/end the VMM3a/SRS data stream
* **Wireshark with ESS Lua plugin** to monitor the data stream
* <span style="color:grey">* **Kafka** to send the online monitoring stream to DAQuiri *</span>
* <span style="color:grey">* **EFU** to perform the online reconstruction for the online monitoring *</span> 
* <span style="color:grey">* **DAQuiri** to take the EFU/Kafka data stream and visualise it *</span> 
* **tcpdump** for the saving of the initial (non-online monitoring) data stream to disk for the offline analysis
* **vmm-sdat** for the reconstruction and the online monitoring


#### Slow control software

The slow control software controls one or several SRS FEC v6 cards.
The VMM3a hybrids have to be connected to a DVMM v5 card.
Keep in mind that two [versions](../versions.md) of the slow control
software exist, which only work with a specific firmware each.

TODO: ADD FIRMWARE VERSIONS THAT SHOULD BE SEEN WHEN CLICKING
SYSTEM PARAMETERS IN THE SLOW CONTROL.

After connecting everything together (cables, hybrids, DVMM, FEC,
computer, etc.) and turning the system on, the reader should wait
around 10 to 15 seconds until the FEC is booted.

The following description uses the small and testing flavour of the
slow control, but it is rather similar for the larger system flavour.

The first thing the reader must do to operate the system is to select
the configuration of FECs and Hybrids in the slow control by clicking
on the corresponding check boxes.
In the given example, 2 hybrids (connected to the DVMM ports 1 and 2)
are used together with 1 FEC:

![](./image_slow-control-window.png)

After selecting the experimental configuration, the
`Open Communication` button in the top left corner has to be
pressed:

![](./image_open-communication.png)

If everything is turned on and connected correctly, the system will
show a green field stating `all alive` after pressing the button.
If it does not work it will show that the `ping failed`.
Then the reader should check the powering, the network connection and ensure
that the network connecting is set up correctly.

Then the `System Parameters` can be read, which gives the firmware version and
the IP addresses. Ensure that the IP address of the FEC is `10.0.0.2` (default)
and for the computer `10.0.0.3`.
The netmask for the computer's network interface is `255.255.255.0`.

![](./image_system-parameters.png)

It can be seen that the slow control software can do more than simply control
the system.
For controlling the system, the `VMM Slow Control` tab has to be used.
To calibrate the hybrids the `Calibration` tab has to be used,
and to test the hybrid quality the `Testing` tab has to be used.

![](./image_tabs.png)

In the following some screenshots are shown to illustrate roughly how the
different tabs look like (improvements in the software may also affect the
style).

**Slow Control tab**
![Slow Control tab](./image_tab_slow-control.png)

**Calibration module**
![Calibration tab](./image_tab_calibration.png)

**Testing module**
![Testing tab](./image_tab_testing.png)

Now, after knowing how everything looks like and having established a connection
between the DAQ computer and the SRS, the reader has to ensure that the hybrids
are recognised by the system and the communication between FEC and hybrids
is synchronised.

For this the user has to press the `Link status` button in the FEC tab of
the VMM slow control:

![](./image_link-status.png)

If the communication is correctly working, the number 4 should occur at the
places where hybrids are connected (see `Data, 5`).
This indicates that everything is working and the system is ready for data
taking.

Please be aware that the order of the places is the other way around.
It is hybrids: `8 7 6 5 4 3 2 1`.
So in the above example it can be seen that the hybrids 1 and 2 have link
status 4, which are exactly the two ports where hybrids where connected.

Link status 0 and 2 means that nothing is connected or that the connected
hybrid is not recognised.
This may indicate a loose HDMI contact, bad grounding or bad powering.

If the link status is 3, it means that the hybrid is recognised by the FEC,
but the communication is not synchronised.
Then press the `Warm Init FEC` button above the `Link Status` button and try
again.
You may also have to press the link status button several times.
If you cannot establish a *4 connection* check the grounding,
the powering or the HDMI contacts.
You may powercycle the system.

Link status 5 means that the system is actually data taking.

Having a link status of 4, it should be ensured that everything works correctly.
For this the hybrid-ID can be read via i2c or the temperature in the
analogue output:

![](./image_i2c.png)

Furthermore, the pedestal can be read in the calibration tab (see screenshot
of the calibration module).

For data taking you can press the `ACQ on/off` button in the FEC tab
(this starts the data taking for each FEC individually and may lead to
asynchronous data) or you press the global `ACQ on/off` buttons,
which is recommended:

![](./image_acq-on-off.png)

!!! warning "Changing settings/reading i2c while `ACQ on`"

    NEVER change any of the settings (it does not matter which one)
    while the `ACQ on` button is active!
    This includes to NEVER press `Warm Init FEC` while `ACQ on` is
    active!
    This will in the best case just automatically stop your data
    taking (for the large systems flavour) or completely destroy your
    data taking (for the small and testing flavour).


!!! note "Pressing `ACQ on/off` too often"

    Do not press `ACQ on/off` too often and too close
    (distance in time).
    You may bring your system in a fuzzy state at some point and
    you have to powercycle your system.


Speaking of the settings: VMM3a/SRS is a highly configurable system.
As written already in the beginning, it is up to the reader to find the optimal
settings for the reader’s measurements.
The settings that are shown in this screenshot are the `default` ones which
*should* work most of the time (no guarantee).
In case you change some of the settings, especially on the VMM level,
it may be useful to reset the VMM.
For this, press the `Hard reset VMM` button at the bottom.
This is particularly useful, when you changed channel settings (enabled test
pulses `ST` or masked channels `SM`).

![](./image_hard-reset.png)

Especially when going to the `Advanced Settings` of the VMM,
it is recommended to be very careful with changing the settings.
So do not be surprised if something happens that you do not understand.

![](./image_adv_set.png)


#### Wireshark
An essential tool that should be installed (even before installing the
slow control) is Wireshark.
Please install Wireshark with Lua support.
In the slow control folder, we provide a Lua script, that disassembles
the UDP packages from the FEC, and displays the hits from the VMMs.
The script vmm3a_plugin.lua is also provided in the slow control
repository.
It might be useful to create an alias in `.bashrc` to start Wireshark
with the script:

```bash
alias essws='wireshark -X lua_script:/path_to_slow_control/vmmsc/vmm3a_plugin.lua'
```


#### Status LEDs on FEC

TO BE ADDED
