# VMM3a/SRS flavours

Currently, two flavours of VMM3a/SRS exist.
There is the *small and testing* for small systems and chip testing and
there is the *large systems* flavour for setups requiring more than
one [FEC](hardware/components.md) with stable clock synchronisation
between the FECs.

!!! note "Which one to pick?"

    Probably you are wondering, which flavour to pick for your system.
    Experience shows that the large systems flavour provides a system
    with a more stable operation, i.e. for the beginning this would
    be recommended.
    It is also the one, which is linked in the [quick start page](https://vmm-srs.docs.cern.ch/quick-start/)

In both cases, slightly different versions of the control software and
the firmware are needed.
For more information on the firmware, please go to the
[corresponding section](firmware.md).
The DAQ, monitoring and reconstruction software remains identical.
Here you can find the corresponding links.

#### Large systems (**44.4444 MHz base clock**, fixed CKBC)
* [Control software](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/tree/large_systems)
  
    &check; calibration procedure for the timewalk.

    &cross; no testing module.

* [FEC firmware](firmware/1e58934_44MHz_FEC.bit)
* [Hybrid firmware](firmware/top_hybrid_20210415.bit)

#### Small and testing (**40 MHz base clock**, adjustable CKBC)
* [Control software](https://gitlab.cern.ch/rd51-slow-control/vmmsc/-/tree/main)

    &cross; calibration procedure for the timewalk.

    &check; testing module for QA purposes.

* [FEC firmware](firmware/fecv6_vmm3_top_21031200.bit)
* [Hybrid firmware](firmware/vmm3h_1_020920_20201118.bit)
